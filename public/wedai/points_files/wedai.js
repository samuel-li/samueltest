/*
* wedai mobile toolkit 
**/

'use strict';
(function (window, undefined) {
	var wedai = function (options) {
		return new wedai.fn.init(options);
	};
	wedai.fn = wedai.prototype = {
		init: function (options) { // 初始化默认设置
			this.name = 'wedai';
			this.version = '0.1';
			this.author = 'eshare';
			this.release = '2015/5/29';
			this.wxApiList = ['checkJsApi',
				'onMenuShareTimeline',
				'onMenuShareAppMessage',
				'onMenuShareQQ',
				'onMenuShareWeibo',
				'hideMenuItems',
				'showMenuItems',
				'hideAllNonBaseMenuItem',
				'showAllNonBaseMenuItem',
				'translateVoice',
				'startRecord',
				'stopRecord',
				'onRecordEnd',
				'playVoice',
				'pauseVoice',
				'stopVoice',
				'uploadVoice',
				'downloadVoice',
				'chooseImage',
				'previewImage',
				'uploadImage',
				'downloadImage',
				'getNetworkType',
				'openLocation',
				'getLocation',
				'hideOptionMenu',
				'showOptionMenu',
				'closeWindow',
				'scanQRCode',
				'chooseWXPay',
				'openProductSpecificView',
				'addCard',
				'chooseCard',
				'openCard'];
			this.sharetypes = (options && options.sharetypes) ? options.sharetypes : ['message', 'timeline', 'qq', 'weibo'];
			return this;
		}
	};
	wedai.extend = function () { // 核心继承方法
		var target = arguments[0] || {},
			i = 1,
			key,
			length = arguments.length,
			deep = false,
			sources;
		if (target.constructor === Boolean) {
			deep = true;
			target = arguments[1] || {};
			i = 2;
		}
		if (typeof(target) !== 'object' && typeof(target) !== 'function') {
			target = {};
		}
		if (length === i) {
			target = this;
			i--;
		}
		for (; i < length; i++) {
			if ((sources = arguments[i]) !== null) {
				for (key in sources) {
					var original = target[key],
						copy = sources[key];
					if (original === copy) {
						continue;
					}
					if (deep && copy && typeof(copy) === 'object' && !copy.nodeType) {
						target[key] = wedai.extend(deep, original || (copy.length !== null ? [] : {}), copy);
					} else if (copy !== undefined) {
						target[key] = copy;
					}
				}
			}
		}
		return target;
	};
	wedai.extend({
		each: function (items, callback){
			var i, key;
			if(items.constructor === Array) {
				for (i = 0; i < items.length; i++) {
					if (callback.call(items[i], i, items[i]) === false) {
						return items;
					}
				}
			} else {
				for (key in items) {
					if (callback.call(items[key], key, items[key]) === false) {
						return items;
					}
				}
			}
    		return items;
		}
	});
	wedai.extend({ // 添加分享方法
		share: function () {
			var options = arguments[0],
				method = arguments[1];
			switch (method) {
				case 'message':
					wx.onMenuShareAppMessage({
						title: options.title,
						desc: options.content,
						link: options.urlShare,
						imgUrl: options.img,
						trigger: function (res) {
							if (options.trigger && typeof(options.trigger === 'function')) {
								options.trigger(res);
							}
						},
						success: function (res) {
							if (options.success && typeof(options.success === 'function')) {
								options.success(res);
							}
						},
						cancel: function (res) {
							if (options.cancel && typeof(options.cancel === 'function')) {
								options.cancel(res);
							}
						},
						fail: function (res) {
							if (options.fail && typeof(options.fail === 'function')) {
								options.fail(res);
							}
						}
					});
				break;
				case 'timeline':
					wx.onMenuShareTimeline({
						title: options.title,
						link: options.urlShare,
						imgUrl: options.img,
						trigger: function (res) {
							if (options.trigger && typeof(options.trigger === 'function')) {
								options.trigger(res);
							}
						},
						success: function (res) {
							if (options.success && typeof(options.success === 'function')) {
								options.success(res);
							}
						},
						cancel: function (res) {
							if (options.cancel && typeof(options.cancel === 'function')) {
								options.cancel(res);
							}
						},
						fail: function (res) {
							if (options.fail && typeof(options.fail === 'function')) {
								options.fail(res);
							}
						}
					});
				break;
				case 'qq':
					wx.onMenuShareQQ({
						title: options.title,
						desc: options.content,
						link: options.urlShare,
						imgUrl: options.img,
						trigger: function (res) {
							if (options.trigger && typeof(options.trigger === 'function')) {
								options.trigger(res);
							}
						},
						success: function (res) {
							if (options.success && typeof(options.success === 'function')) {
								options.success(res);
							}
						},
						cancel: function (res) {
							if (options.cancel && typeof(options.cancel === 'function')) {
								options.cancel(res);
							}
						},
						fail: function (res) {
							if (options.fail && typeof(options.fail === 'function')) {
								options.fail(res);
							}
						}
					});
				break;
				case 'weibo':
					wx.onMenuShareWeibo({
						title: options.title,
						desc: options.content,
						link: options.urlShare,
						imgUrl: options.img,
						trigger: function (res) {
							if (options.trigger && typeof(options.trigger === 'function')) {
								options.trigger(res);
							}
						},
						success: function (res) {
							if (options.success && typeof(options.success === 'function')) {
								options.success(res);
							}
						},
						cancel: function (res) {
							if (options.cancel && typeof(options.cancel === 'function')) {
								options.cancel(res);
							}
						},
						fail: function (res) {
							if (options.fail && typeof(options.fail === 'function')) {
								options.fail(res);
							}
						}
					});
				break;
			}
		}
	});
	wedai.extend({ // 添加微信事件代理
		on: function () {

		}
	});
	wedai.extend({ // 添加微信menuShare事件代理
		onMenuShare: function () {
			var options = arguments[0],
				methods = arguments[1],
				i = 0,
				length = arguments.length;
			if (length === 0) {
				options = wedai.shareData;
				methods = ['message', 'timeline', 'qq', 'weibo'];
			}
			if (methods && typeof(methods) === 'string') {
				methods = [methods];
			} else if (!methods || methods.constructor !== Array) {
				methods = ['message', 'timeline', 'qq', 'weibo'];
			}
			for (; i < methods.length; i++) {
				wedai.share(options, methods[i]);
			}
		}
	});
	wedai.extend({ // 默认分享数据
		shareData: {
			title: '加入易享微代，一起赚钱，一起任性',
			content: '免费开店，海量货源，每天分享店铺和产品，丰富佣金等你赚！',
			urlShare: 'http://m.wedai.net/passport/login/',
			img: 'http://m.wedai.net/template/v20/img/default/avatar.png'
		}
	});
	wedai.extend({
		config: wedai.config ? wedai.config : wedai()
	});
	window.wedai = window.wd = wedai;
})(window);