<?php

namespace App\Http\Controllers\wedai;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use App\Http\Models\WdStoreTaskRecord;
use App\Http\Controllers\wedai\SecStrategy;

/**
 * Class pointsController
 * @package App\Http\Controllers\wedai
 * 1. 生成安全码
 *    生成加密的安全码
 *    生成解密安全码的JS
 * 2. 安全码记录到session里
 * 3. 客户端页面用解密安全码的JS解密安全码并发起请求
 * 4. 判断客户端来自手机
 * 5. 判断安全码是否正确
 * 6. 判断页面停留时间 >5
 * 7. 同一用户ID只能访问一次
 */
class pointsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->session()->put('start_time', time());
        $request->session()->put('user_hash_id', '12345678');
        $secStrategy = new SecStrategy();
        $secContent = $secStrategy->getStrategy();
        $request->session()->put('secCode', $secContent['secCode']);
        // [secCode='s1', encSecCode='t2', decJsCode='xxxx']
        return view('wedai/points', array_merge($secContent, array('task_agent_id'=>1)));
    }

    protected function getJsStrategy() {

    }

    public function getJoinCode(Request $request, $seccode, $task_agent_id)
    {
        if (!$this->isMobile() || $seccode !== $request->session()->get('secCode')) {
            $jsBody = '$("#footer").html("亲~ 请从手机访问哦");';
            $ret = array('jsCode'=>$jsBody);
            return $this->response($request, $ret);
        }
        $user_hash_id = $request->session()->get('user_hash_id');
        $records = WdStoreTaskRecord::where('task_agent_id',$task_agent_id)->
            where('user_hash_id',$user_hash_id)->get();

        $start_time = $request->session()->get('start_time');
        if (($request->session()->has('start_time') && time()-$start_time > 5) || $records->count()>0) {
            $jsBody = '
                $("#footer").html("亲~ 感谢您的参与!~");
                $("a").css("cursor", "pointer");
                enableLink=true;
            ';
            // TODO: set a validate PV
            if ($records->count() == 0) {
                $taskR = new WdStoreTaskRecord();
                $taskR->task_agent_id = $task_agent_id;
                $taskR->user_hash_id = $user_hash_id;
                $taskR->ip_address = $_SERVER["REMOTE_ADDR"];
                $taskR->create_at = time();
                $taskR->save();
            }
        } else {
            $jsBody = '$("#footer").html("亲~ 恳请您再仔细看一下哦~");
                window.setTimeout(getJoinCode,1500);';
        }
        $ret = array('jsCode'=>$jsBody);
        return $this->response($request, $ret);
    }

    protected function response(Request $request, $content) {
        if ($request->has('callback')) {
            return  Response::jsonp(Request::input('callback'), $content);
        } else {
            return Response::json($content);
        }
    }

    protected  function isMobile()
    {
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
            $clientkeywords = array(
                'nokia', 'sony', 'ericsson', 'mot', 'samsung', 'htc', 'sgh', 'lg', 'sharp', 'sie-',
                'philips', 'panasonic', 'alcatel', 'lenovo', 'iphone', 'ipod', 'blackberry', 'meizu',
                'android', 'netfront', 'symbian', 'ucweb', 'windowsce', 'palm', 'operamini',
                'operamobi', 'opera mobi', 'openwave', 'nexusone', 'cldc', 'midp', 'wap', 'mobile',
                'micromessenger'
            );
            if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", $userAgent) && strpos($userAgent, 'ipad') === false) {
                return true;
            }
        }
        return false;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
