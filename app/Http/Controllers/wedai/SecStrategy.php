<?php
namespace App\Http\Controllers\wedai;

class SecStrategy{
    public function getStrategy() {
        $secFunc = $this->getSecFunc();
        return $this->$secFunc();
    }

    protected function getSecCode() {
        $preStr = "a1bcd3ef2ghi3jk4l4mn1a4e";
        $len = strlen($preStr);
        $encA = array();
        for($idx = 0; $idx < $len; $idx ++) {
            $encA[] = $preStr[$idx];
        }
        shuffle($encA);
        return $encA[0].$encA[1].$encA[2].$encA[3];
    }

    protected function getSecFunc() {
        $links = array('s1','s1a', 's2','s2a','s3','s3a');
        shuffle($links);
        return $links[0];
    }

    protected function s1() {
        $SecCode = $this->getSecCode();
        $encSecCode = chr(ord($SecCode[0])+1).chr(ord($SecCode[1])+1).chr(ord($SecCode[2])+1).chr(ord($SecCode[3])+1);
        $decJsCode = 'return String.fromCharCode(li.charAt(0).charCodeAt()-1)+String.fromCharCode(li.charAt(1).charCodeAt()-1)+String.fromCharCode(li.charAt(2).charCodeAt()-1)+String.fromCharCode(li.charAt(3).charCodeAt()-1);';
        return array('secCode'=>$SecCode,'encSecCode'=>$encSecCode,'decJsCode'=>$decJsCode);
    }
    protected function s1a() {
        $SecCode = $this->getSecCode();
        $encSecCode = chr(ord($SecCode[0])-1).chr(ord($SecCode[1])-1).chr(ord($SecCode[2])-1).chr(ord($SecCode[3])-1);
        $decJsCode = 'return String.fromCharCode(li.charAt(0).charCodeAt()+1)+String.fromCharCode(li.charAt(1).charCodeAt()+1)+String.fromCharCode(li.charAt(2).charCodeAt()+1)+String.fromCharCode(li.charAt(3).charCodeAt()+1);';
        return array('secCode'=>$SecCode,'encSecCode'=>$encSecCode,'decJsCode'=>$decJsCode);
    }

    protected function s2() {
        $SecCode = $this->getSecCode();
        $encSecCode = chr(ord($SecCode[0])+2).chr(ord($SecCode[1])+2).chr(ord($SecCode[2])+2).chr(ord($SecCode[3])+2);
        $decJsCode = 'return String.fromCharCode(li.charAt(0).charCodeAt()-2)+String.fromCharCode(li.charAt(1).charCodeAt()-2)+String.fromCharCode(li.charAt(2).charCodeAt()-2)+String.fromCharCode(li.charAt(3).charCodeAt()-2);';
        return array('secCode'=>$SecCode,'encSecCode'=>$encSecCode, 'decJsCode'=>$decJsCode);
    }

    protected function s2a() {
        $SecCode = $this->getSecCode();
        $encSecCode = chr(ord($SecCode[0])-2).chr(ord($SecCode[1])-2).chr(ord($SecCode[2])-2).chr(ord($SecCode[3])-2);
        $decJsCode = 'return String.fromCharCode(li.charAt(0).charCodeAt()+2)+String.fromCharCode(li.charAt(1).charCodeAt()+2)+String.fromCharCode(li.charAt(2).charCodeAt()+2)+String.fromCharCode(li.charAt(3).charCodeAt()+2);';
        return array('secCode'=>$SecCode,'encSecCode'=>$encSecCode, 'decJsCode'=>$decJsCode);
    }

    protected function s3() {
        $SecCode = $this->getSecCode();
        $encSecCode = chr(ord($SecCode[0])+3).chr(ord($SecCode[1])+3).chr(ord($SecCode[2])+3).chr(ord($SecCode[3])+3);
        $decJsCode = 'return String.fromCharCode(li.charAt(0).charCodeAt()-3)+String.fromCharCode(li.charAt(1).charCodeAt()-3)+String.fromCharCode(li.charAt(2).charCodeAt()-3)+String.fromCharCode(li.charAt(3).charCodeAt()-3);';
        return array('secCode'=>$SecCode,'encSecCode'=>$encSecCode, 'decJsCode'=>$decJsCode);
    }

    protected function s3a() {
        $SecCode = $this->getSecCode();
        $encSecCode = chr(ord($SecCode[0])-3).chr(ord($SecCode[1])-3).chr(ord($SecCode[2])-3).chr(ord($SecCode[3])-3);
        $decJsCode = 'return String.fromCharCode(li.charAt(0).charCodeAt()+3)+String.fromCharCode(li.charAt(1).charCodeAt()+3)+String.fromCharCode(li.charAt(2).charCodeAt()+3)+String.fromCharCode(li.charAt(3).charCodeAt()+3);';
        return array('secCode'=>$SecCode,'encSecCode'=>$encSecCode, 'decJsCode'=>$decJsCode);
    }
}