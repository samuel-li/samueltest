<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class WdStoreTaskRecord extends Model
{
    protected $table = 'wd_store_task_record';
    public $timestamps = false;

    public function parent()
    {
        return $this->belongsTo('App\Http\Models\WdStoreTaskAgent', 'task_agent_id', 'task_agent_id');
    }
}
