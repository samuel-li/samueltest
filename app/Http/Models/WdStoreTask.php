<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class WdStoreTask extends Model
{
    protected $table = 'wd_store_task';
    public $timestamps = false;

    public function childs()
    {
        return $this->hasMany('App\Http\Models\WdStoreTaskDaily', 'task_id', 'task_id');
    }
}
