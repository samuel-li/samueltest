<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class WdStoreTaskDaily extends Model
{
    protected $table = 'wd_store_task_daily';
    public $timestamps = false;

    public function parent()
    {
        return $this->belongsTo('App\Http\Models\WdStoreTask', 'task_id', 'task_id');
    }

    public function childs()
    {
        return $this->hasMany('App\Http\Models\WdStoreTaskAgent', 'task_daily_id', 'task_daily_id');
    }
}
