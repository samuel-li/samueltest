<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class WdStoreTaskAgent extends Model
{
    protected $table = 'wd_store_task_agent';
    public $timestamps = false;

    public function parent()
    {
        return $this->belongsTo('App\Http\Models\WdStoreTaskDaily', 'task_daily_id', 'task_daily_id');
    }

    public function childs()
    {
        return $this->hasMany('App\Http\Models\WdStoreTaskRecord', 'task_agent_id', 'task_agent_id');
    }
}
