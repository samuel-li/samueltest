<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'indexController@index');
Route::get('/wedai/points', 'wedai\pointsController@index');
Route::get('/wedai/points/join/{secCode}/{task_agent_id}', 'wedai\pointsController@getJoinCode');
